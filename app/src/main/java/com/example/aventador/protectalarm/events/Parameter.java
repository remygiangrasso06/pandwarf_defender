package com.example.aventador.protectalarm.events;

/**
 * Created by Aventador on 22/09/2017.
 */

public enum Parameter {
    FREQUENCY,
    RSSI_VALUE,
    PEAK_TOLERANCE,
    MARGIN_ERROR,
    DATE;
}
