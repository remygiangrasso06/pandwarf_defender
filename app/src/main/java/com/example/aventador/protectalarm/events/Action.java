package com.example.aventador.protectalarm.events;

/**
 * Created by Aventador on 21/09/2017.
 */

public enum Action {
    // ========= ACTION ===========
    CONNECT,
    DISCONNECT,
    STOP_CONNECT,
    UPDATE,
    START_SEARCH_THRESHOLD,
    STOP_SEARCH_THRESHOLD,
    START_PROTECTION,
    STOP_PROTECTION,
    START_JAMMING,
    STOP_JAMMING,
}
